﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pembekalan_net.api.Services;
using pembekalan_net.datamodels;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        //private readonly pembekalan_netContext db;
        private VMResponse respon = new VMResponse();
        private RolesService rolesService;

        //public apiRoleController(pembekalan_netContext _db)
        public apiRoleController(RolesService _rolesService)
        {
            //db = _db;
            //rolesService = new RolesService(db);
            rolesService = _rolesService;
        }

        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData()
        {
            List<TblRole> data = rolesService.GetAllData();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole DataById(int id)
        {
            TblRole data = rolesService.GetDataById(id);
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblRole data)
        {
            VMResponse respon = rolesService.Save(data);
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            VMResponse respon = rolesService.Edit(data);
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            VMResponse respon = rolesService.Delete(id);
            return respon;
        }

        [HttpGet("CheckNameIsExist/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            bool result = rolesService.CheckNameIsExist(name, id);
            return result;
        }


    }
}
