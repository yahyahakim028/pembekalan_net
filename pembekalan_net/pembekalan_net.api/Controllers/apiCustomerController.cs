﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pembekalan_net.api.Services;
using pembekalan_net.datamodels;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerController : ControllerBase
    {
        //private readonly pembekalan_netContext db;
        private VMResponse respon = new VMResponse();
        private CustomerService customerService;

        //public apiCustomerController(pembekalan_netContext _db)
        public apiCustomerController(CustomerService _customerService)
        {
            //db = _db;
            //customerService = new CustomerService(db);
            customerService = _customerService;
        }

        [HttpGet("GetAllData")]
        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = customerService.GetAllData();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = customerService.GetDataById(id);
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCustomer data)
        {
            VMResponse respon = customerService.Save(data);
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCustomer data)
        {
            VMResponse respon = customerService.Edit(data);
            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            VMResponse respon = customerService.Delete(id);
            return respon;
        }

        [HttpGet("CheckEmailIsExist/{email}/{id}")]
        public bool CheckEmail(string email, int id)
        {
            bool result = customerService.CheckEmailIsExist(email, id);
            return result;
        }

        [HttpPut("MultipleDelete")]
        public VMResponse MultipleDelete(List<int> listId)
        {
            VMResponse respon = customerService.MultipleDelete(listId);
            return respon;
        }

    }
}
