﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using pembekalan_net.api.Services;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAuthController : ControllerBase
    {
        private VMResponse respon = new VMResponse();
        private AuthService authService;

        public apiAuthController(AuthService _authService)
        {
            authService = _authService;
        }

        [HttpGet("CheckLogin/{email}/{password}")]
        public VMTblCustomer CheckLogin(string email, string password)
        {
            VMTblCustomer data = authService.CheckLogin(email, password);
            return data;
        }

        [HttpGet("MenuAccess/{IdRole}")]
        public List<VMMenuAccess> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> data = authService.MenuAccess(IdRole);
            return data;
        }




    }
}
