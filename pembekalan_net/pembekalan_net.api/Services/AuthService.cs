﻿using pembekalan_net.datamodels;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Services
{
    public class AuthService
    {
        private readonly pembekalan_netContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public AuthService(pembekalan_netContext _db)
        {
            db = _db;
        }

        public VMTblCustomer CheckLogin(string email, string password)
        {
            VMTblCustomer data = (from v in db.TblCustomers
                                        join c in db.TblRoles on v.IdRole equals c.Id
                                        where v.IsDelete == false && v.Email.Equals(email) && v.Password.Equals(password)
                                        select new VMTblCustomer
                                        {
                                            Id = v.Id,
                                            NameCustomer = v.NameCustomer,
                                            IdRole = v.IdRole,
                                            RoleName = c.RoleName,
                                        }
                                        ).FirstOrDefault()!;
            return data;
        }

        public List<VMMenuAccess> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> listMenu = (from parent in db.TblMenus
                                           join ma in db.TblMenuAccesses on parent.Id equals ma.MenuId
                                           where parent.IsParent == true && ma.IdRole == IdRole
                                           && parent.IsDelete == false && ma.IsDelete == false
                                           select new VMMenuAccess
                                           {
                                               Id = parent.Id,
                                               MenuName = parent.MenuName,
                                               MenuAction = parent.MenuAction,
                                               MenuController = parent.MenuController,
                                               MenuIcon = parent.MenuIcon,
                                               IdRole = ma.IdRole,
                                               MenuSorting = parent.MenuSorting,
                                               ListChild = (from child in db.TblMenus
                                                            join ma2 in db.TblMenuAccesses on child.Id equals ma2.MenuId
                                                            where child.MenuParent == parent.Id && child.IsDelete == false
                                                            && ma2.IsDelete == false && ma2.IdRole == IdRole
                                                            select new VMMenuAccess
                                                            {
                                                                Id = child.Id,
                                                                MenuName = child.MenuName,
                                                                MenuAction = child.MenuAction,
                                                                MenuController = child.MenuController,
                                                                MenuIcon = child.MenuIcon,
                                                                IdRole = IdRole,
                                                                MenuSorting = child.MenuSorting,
                                                                MenuParent = child.MenuParent,

                                                            }).OrderBy(a => a.MenuSorting).ToList()

                                           }).OrderBy(a => a.MenuSorting).ToList();

            return listMenu;
        }


    }
}
