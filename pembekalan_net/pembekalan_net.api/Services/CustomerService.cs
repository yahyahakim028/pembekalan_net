﻿using pembekalan_net.datamodels;
using pembekalan_net.libraries;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Services
{
    public class CustomerService
    {
        private readonly pembekalan_netContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public CustomerService(pembekalan_netContext _db)
        {
            db = _db;
        }

        public List<VMTblCustomer> GetAllData()
        {
            List<VMTblCustomer> data = (from v in db.TblCustomers
                                        join c in db.TblRoles on v.IdRole equals c.Id
                                        where v.IsDelete == false
                                        select new VMTblCustomer
                                        {
                                            Id = v.Id,
                                            NameCustomer = v.NameCustomer,
                                            Email = v.Email,
                                            Password = v.Password,
                                            Address = v.Address,
                                            Phone = v.Phone,

                                            IdRole = v.IdRole,
                                            RoleName = c.RoleName,

                                            IsDelete = v.IsDelete,
                                            CreateDate = v.CreateDate
                                        }
                                        ).ToList();
            return data;
        }

        public VMTblCustomer GetDataById(int id)
        {
            VMTblCustomer data = (from v in db.TblCustomers
                                        join c in db.TblRoles on v.IdRole equals c.Id
                                        where v.IsDelete == false && v.Id == id
                                        select new VMTblCustomer
                                        {
                                            Id = v.Id,
                                            NameCustomer = v.NameCustomer,
                                            Email = v.Email,
                                            Password = v.Password,
                                            Address = v.Address,
                                            Phone = v.Phone,

                                            IdRole = v.IdRole,
                                            RoleName = c.RoleName,

                                            IsDelete = v.IsDelete,
                                            CreateDate = v.CreateDate
                                        }
                                        ).FirstOrDefault()!;
            return data;
        }

        public VMResponse Save(TblCustomer data)
        {
            try
            {
                //data.Password = EncryptDecryptManager.Encrypt(data.Password);
                data.Password = EncryptDecryptManager.hashPassword(data.Password);
                data.CreateBy = IdUser;
                data.CreateDate = DateTime.Now;
                data.IsDelete = false;

                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        public VMResponse Edit(TblCustomer data)
        {
            TblCustomer dataModel = db.TblCustomers.Find(data.Id)!;

            try
            {
                dataModel.IdRole = data.IdRole;
                dataModel.NameCustomer = data.NameCustomer;
                dataModel.Email = data.Email;
                dataModel.Password = data.Password;
                dataModel.Address = data.Address;
                dataModel.Phone = data.Phone;
                dataModel.UpdateBy = IdUser;
                dataModel.UpdateDate = DateTime.Now;

                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        public VMResponse Delete(int id)
        {
            TblCustomer dataModel = db.TblCustomers.Find(id)!;

            try
            {
                dataModel.IsDelete = true;
                dataModel.UpdateBy = IdUser;
                dataModel.UpdateDate = DateTime.Now;

                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success deleted";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed deleted : " + ex.Message;
            }

            return respon;
        }

        public bool CheckEmailIsExist(string email, int id)
        {
            TblCustomer data = new TblCustomer();

            data = db.TblCustomers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault()!;

            if (data != null && data.Id != id)
            {
                return true;
            }

            return false;
        }

        public VMResponse MultipleDelete(List<int> listId)
        {
            try
            {
                foreach (int id in listId)
                {
                    TblCustomer dataModel = db.TblCustomers.Find(id)!;
                    dataModel.IsDelete = true;
                    dataModel.UpdateBy = IdUser;
                    dataModel.UpdateDate = DateTime.Now;

                    db.Update(dataModel);
                }

                db.SaveChanges();

                respon.Message = "Data success deleted";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed deleted : " + ex.Message;
            }

            return respon;
        }


    }
}
