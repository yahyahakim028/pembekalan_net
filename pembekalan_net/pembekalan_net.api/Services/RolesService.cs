﻿using pembekalan_net.datamodels;
using pembekalan_net.viewmodels;

namespace pembekalan_net.api.Services
{
    public class RolesService
    {
        private readonly pembekalan_netContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public RolesService(pembekalan_netContext _db)
        {
            db = _db;
        }

        public List<TblRole> GetAllData()
        {
            List<TblRole> data = db.TblRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        public TblRole GetDataById(int id)
        {
            TblRole data = db.TblRoles.Find(id)!;
            return data;
        }

        public VMResponse Save(TblRole data)
        {
            data.CreatedBy = IdUser;
            data.CreatedDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch(Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        public VMResponse Edit(TblRole data)
        {
            TblRole dataModel = db.TblRoles.Find(data.Id)!;

            try
            {
                dataModel.RoleName = data.RoleName;
                dataModel.UpdatedBy = IdUser;
                dataModel.UpdatedDate = DateTime.Now;

                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch(Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        public VMResponse Delete(int id)
        {
            TblRole dataModel = db.TblRoles.Find(id)!;

            try
            {
                dataModel.IsDelete = true;
                dataModel.UpdatedBy = IdUser;
                dataModel.UpdatedDate = DateTime.Now;

                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success deleted";
            }
            catch(Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed deleted : " + ex.Message;
            }

            return respon;
        }

        public bool CheckNameIsExist(string name, int id)
        {
            TblRole data = new TblRole();

            data = db.TblRoles.Where(a=> a.RoleName == name && a.IsDelete == false).FirstOrDefault()!;

            if(data != null && data.Id != id)
            {
                return true;
            }

            return false;
        }


    }
}
