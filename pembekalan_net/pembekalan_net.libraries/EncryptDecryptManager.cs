﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace pembekalan_net.libraries
{
    public class EncryptDecryptManager
    {
        //Use Scrypt(Cryptography) or Bscrypt

        //Security Cryptography Encrypt Decrypt (AES / Advanced Encryption Standard)
        public readonly static string key = "HelloWorld!00000";
        //public readonly static string key = "ashprogheldotnetmania2022key123";
        //public readonly static string key = "pendalamanclassdotnet2024";
        //public readonly static string key = "bootcampclassdotnet2024";

        public static string Encrypt(string text)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using(Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using(MemoryStream ms = new MemoryStream())
                {
                    using(CryptoStream cs = new CryptoStream((Stream)ms, encryptor, CryptoStreamMode.Write))
                    {
                        using(StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(text);
                        }
                        array = ms.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(array);
        }

        public static string Decrypt(string text)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(text);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream ms = new MemoryStream(buffer))
                {
                    using (CryptoStream cs = new CryptoStream((Stream)ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader sr = new StreamReader(cs))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }
        }


        //-----


        //Security Cryptography SHA256
        public static string hashPassword(string text)
        {
            var sha = SHA256.Create();
            var byteArray = Encoding.Default.GetBytes(text);

            var hashedPassword = sha.ComputeHash(byteArray);
            return Convert.ToBase64String(hashedPassword);
        }

    }
}
