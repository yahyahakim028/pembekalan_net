﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pembekalan_net.viewmodels
{
    public class VMTblRole
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Harap isi RoleName!!")]
        [StringLength(10, ErrorMessage = "Max panjang karakter adalah 10")]
        //[MaxLength(5)]
        public string? RoleName { get; set; }
        public bool? IsDelete { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
