﻿using Microsoft.AspNetCore.Mvc;
using pembekalan_net.datamodels;
using pembekalan_net.Services;
using pembekalan_net.viewmodels;

namespace pembekalan_net.Controllers
{
    public class RoleController : Controller
    {
        //private readonly pembekalan_netContext db;
        private readonly RoleService roleService;

        //public RoleController(pembekalan_netContext _db)
        public RoleController(RoleService _roleService)
        {
            roleService = _roleService;
            //db = _db;
            //roleService = new RoleService(db);
        }

        public IActionResult Index()
        {
            List<VMTblRole> dataView = roleService.GetAllData();
            ViewBag.listRole = dataView;
            return View(dataView);
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(VMTblRole dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = roleService.Create(dataView);

                if (respon.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblRole dataView)
        {
            VMResponse respon = new VMResponse();

            if (ModelState.IsValid)
            {
                respon = roleService.Edit(dataView);

                if (respon.Success)
                {
                    return Redirect("/Role/Index");
                }
            }

            respon.Entity = dataView;
            return View(respon.Entity);
        }

        public IActionResult Detail(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }

        public IActionResult Delete(int id)
        {
            VMTblRole dataView = roleService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Delete(VMTblRole dataView)
        {
            VMResponse respon = new VMResponse();

            respon = roleService.Delete(dataView);

            if (respon.Success)
            {
                return Redirect("/Role/Index");
            }

            return View(respon.Entity);
        }


    }
}
