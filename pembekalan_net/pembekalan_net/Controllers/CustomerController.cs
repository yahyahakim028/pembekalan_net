﻿using Microsoft.AspNetCore.Mvc;
using pembekalan_net.datamodels;
using pembekalan_net.Services;
using pembekalan_net.viewmodels;

namespace pembekalan_net.Controllers
{
    public class CustomerController : Controller
    {
        private Role2Service role2Service;
        private CustomerService customerService;

        public CustomerController(Role2Service _role2Service, CustomerService _customerService)
        {
            role2Service = _role2Service;
            customerService = _customerService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString,
                                                string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            sortOrder = sortOrder ?? "";
            ViewBag.NameSort = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.CustomerSort = sortOrder == "customer" ? "customer_desc" : "customer";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblCustomer> data = await customerService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCustomer.ToLower().Contains(searchString.ToLower())
                    || a.Email.ToLower().Contains(searchString.ToLower())
                    || a.RoleName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            switch (sortOrder)
            {
                case "customer_desc":
                    data = data.OrderByDescending(a => a.NameCustomer).ToList();
                    break;
                case "customer":
                    data = data.OrderBy(a => a.NameCustomer).ToList();
                    break;
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                case "name":
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginatedList<VMTblCustomer>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            List<TblRole> listRole = await role2Service.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView();
        }

        //public async Task<IActionResult> CheckEmailIsExist(string email, int id) //Sama saja
        public async Task<JsonResult> CheckEmailIsExist(string email, int id)
        {
            bool isExist = await customerService.CheckEmailIsExist(email, id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);

            List<TblRole> listRole = await role2Service.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        {
            VMResponse respon = await customerService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await customerService.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> MultipleDelete(List<int> listId)
        {
            List<string> listName = new List<string>();
            foreach (int item in listId)
            {
                VMTblCustomer data = await customerService.GetDataById(item);
                listName.Add(data.NameCustomer);
            }
            ViewBag.ListName = listName;

            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> SureMultipleDelete(List<int> listId)
        {
            VMResponse respon = await customerService.MultipleDelete(listId);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }



    }
}
