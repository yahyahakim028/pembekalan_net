﻿using Microsoft.AspNetCore.Mvc;
using pembekalan_net.datamodels;
using pembekalan_net.Services;
using pembekalan_net.viewmodels;

namespace pembekalan_net.Controllers
{
    public class Role2Controller : Controller
    {
        private Role2Service role2Service;

        public Role2Controller(Role2Service _role2Service)
        {
            role2Service = _role2Service;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString,
                                                string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if(searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<TblRole> data = await role2Service.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.RoleName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.RoleName).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.RoleName).ToList();
                    break;
            }

            return View(PaginatedList<TblRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckNameIsExist(string roleName, int id)
        {
            bool isExist = await role2Service.CheckNameIsExist(roleName, id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TblRole dataParam)
        {
            VMResponse respon = await role2Service.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await role2Service.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TblRole dataParam)
        {
            VMResponse respon = await role2Service.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await role2Service.GetDataById(id);
            return PartialView(data);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await role2Service.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await role2Service.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }


    }
}
