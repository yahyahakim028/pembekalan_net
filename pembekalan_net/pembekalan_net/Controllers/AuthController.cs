﻿using Microsoft.AspNetCore.Mvc;
using pembekalan_net.datamodels;
using pembekalan_net.Services;
using pembekalan_net.viewmodels;

namespace pembekalan_net.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;
        private Role2Service role2Service;
        VMResponse respon = new VMResponse();

        public AuthController(Role2Service _role2Service, AuthService _authService)
        {
            role2Service = _role2Service;
            authService = _authService;
        }

        public IActionResult Login()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> LoginSubmit(string email, string password)
        {
            VMTblCustomer customer = await authService.CheckLogin(email, password);

            if(customer != null)
            {
                respon.Message = $"Hello, {customer.NameCustomer} Welcome to Pembekalan .Net";
                HttpContext.Session.SetString("NameCustomer", customer.NameCustomer);
                HttpContext.Session.SetInt32("IdCustomer", customer.Id);
                HttpContext.Session.SetInt32("IdRole", customer.IdRole ?? 0);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"Oops, {email} not found or password is wrong, please check it!";
            }

            return Json(new { dataRespon = respon });
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Register()
        {
            List<TblRole> listRole = await role2Service.GetAllData();
            ViewBag.ListRole = listRole;

            return PartialView();
        }


    }
}
