﻿using AutoMapper;
using pembekalan_net.datamodels;
using pembekalan_net.viewmodels;

namespace pembekalan_net.Services
{
    public class RoleService
    {
        private readonly pembekalan_netContext db;
        VMResponse respon = new VMResponse();
        int IdUser = 1;

        public RoleService(pembekalan_netContext _db)
        {
            db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblRole, VMTblRole>();
                cfg.CreateMap<VMTblRole, TblRole>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public List<VMTblRole> GetAllData()
        {
            List<VMTblRole> data = new List<VMTblRole>();
            List<TblRole> datamodel = db.TblRoles.Where(a => a.IsDelete == false).ToList();

            data = GetMapper().Map<List<VMTblRole>>(datamodel);

            //foreach (TblRole role in datamodel)
            //{
            //    VMTblRole item = new VMTblRole();
            //    item.Id = role.Id;
            //    item.RoleName = role.RoleName;
            //    item.IsDelete = role.IsDelete;
            //    item.CreatedBy = role.CreatedBy;
            //    item.CreatedDate = role.CreatedDate;
            //    item.UpdatedBy = role.UpdatedBy;
            //    item.UpdatedDate = role.UpdatedDate;

            //    data.Add(item);
            //}

            return data;
        }

        public VMResponse Create(VMTblRole dataView)
        {
            TblRole dataModel = GetMapper().Map<TblRole>(dataView);
            dataModel.IsDelete = false;
            dataModel.CreatedBy = IdUser;
            dataModel.CreatedDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = dataModel;
            }
            catch(Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

        public VMTblRole GetById(int id)
        {
            //TblRole dataModel = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            TblRole dataModel = db.TblRoles.Find(id)!;
            VMTblRole dataView = GetMapper().Map<VMTblRole>(dataModel);
            return dataView;
        }

        public VMResponse Edit(VMTblRole dataView)
        {
            TblRole dataModel = db.TblRoles.Find(dataView.Id)!;
            dataModel.RoleName = dataView.RoleName;
            dataModel.UpdatedBy = IdUser;
            dataModel.UpdatedDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success saved";
                respon.Entity = dataModel;
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

        public VMResponse Delete(VMTblRole dataView)
        {
            TblRole dataModel = db.TblRoles.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdatedBy = IdUser;
            dataModel.UpdatedDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data success deleted";
                respon.Entity = dataModel;
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed deleted : " + ex.Message;
                respon.Entity = dataView;
            }

            return respon;
        }

    }
}
