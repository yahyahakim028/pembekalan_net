

--STORED PROCEDURE(SP)
--create stored procedure (SP)
create procedure sp_RetieveMahasiswa
	--parameter disini
as
begin
	select name,address,email,nilai from mahasiswa
end

--run / exec procedure
exec sp_RetieveMahasiswa
execute sp_RetieveMahasiswa


--edit / alter strored procedure
alter procedure sp_RetieveMahasiswa
	--parameter disini
	@id int,
	@nama varchar(50)
as
begin
	select name,address,email,nilai from mahasiswa
	where id = @id and name = @nama
end

--run / exec procedure
exec sp_RetieveMahasiswa 1,'haikal'
execute sp_RetieveMahasiswa 1,'haikal'


--FUNCTION
--create function
create function fn_total_mahasiswa
(
	@id int
)
RETURNS INT
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) from mahasiswa where id = @id
	return @hasil
END

--run function
select dbo.fn_total_mahasiswa(1)

--edit / alter function
alter function fn_total_mahasiswa
(
	@id int,
	@nama varchar(50)
)
RETURNS INT
BEGIN
	DECLARE @hasil int
	SELECT @hasil = count(id) from mahasiswa where id = @id and name = @nama
	return @hasil
END

--run function
select dbo.fn_total_mahasiswa(1,'haikal')



--duplikat table
select * into tbltes from TblRole
select * from tbltes

--table temporary (bisa pakai into / create)
select * into #tbltes2 from TblRole
select * from #tbltes2

create table #tbltes3(nomor int not null)
select * from #tbltes3


--while loop in SQL
declare @start int, @finish int
declare @result varchar(100)
set @result = ''
set @start = 1
set @finish = 10
while @start <= @finish
begin
	--print @start
	set @result += cast(@start as varchar(3)) + ' '
	set @start = @start + 1
end
--select @result
print @result

------
declare @start int, @finish int
create table #tbltes3(nomor int not null)
--set @start = 1
--set @finish = 10
select @start = 1, @finish = 10
while @start <= @finish
begin
	--print @start
	insert into #tbltes3 values(@start)
	set @start = @start + 1
end
select * from #tbltes3
drop table #tbltes3


--exists
if exists(select * from TblCustomer where id = 1)
begin
	select 'MASUK'
end
else
begin
	select 'GAGAL'
end

--union (gabungin hasil query)
select GETDATE()
union
select GETUTCDATE()


--subquery
select * from (
select GETDATE() as Tanggal,'A'Kode
union
select GETUTCDATE() as Tanggal,'B'Kode
) tbltmp
order by Kode desc


--try catch
declare @nilai1 int, @nilai2 int, @hasil int
select @nilai1 = 5, @nilai2 = 0

begin try
	select @hasil = @nilai1 / @nilai2
	select @hasil
	--print @hasil
end try
begin catch
	select 'Error'
	--print 'Error'
end catch


